using NUnit.Framework;
using ProjetoNUnit.Bases;
using ProjetoNUnit.Pages;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using OpenQA.Selenium.Support.UI;
namespace ProjetoNUnit.Tests
{
    [TestFixture]
    public class FluxoPageTests : TestBase
    {


        #region Pages and Flows Objects
        FluxoPage fluxoPageTests;
        static IWebDriver driver = new ChromeDriver();

        #endregion

        [Test]
        public void RealizarFluxoComSucesso()
        {
            fluxoPageTests = new FluxoPage();
            //loginPage = new LoginPage();
            //mainPage = new MainPage();

            #region Parameters

            string usuario = "guilherme.santos@base2.com.br";
            string senha = "casa1935";
            #endregion

            fluxoPageTests.AcessarUrlSite();
            fluxoPageTests.PreencherUsuario(usuario);
            fluxoPageTests.PreencherSenha(senha);
            fluxoPageTests.ClicarEmLogin();
            fluxoPageTests.ClicarEmMenuButton();
            fluxoPageTests.ClicarEmProjetosButton();
            fluxoPageTests.ClicarEmProjetosCriadosButton();
            fluxoPageTests.ClicarEmCasosTesteButton();

            Assert.IsTrue(fluxoPageTests.ClicarEmRowButton().Displayed);

            fluxoPageTests.Quit();

        }
        
    }

}
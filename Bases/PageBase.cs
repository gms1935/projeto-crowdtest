using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using System.Threading;


namespace ProjetoNUnit.Bases
{
    public class PageBase
    {
        #region Objects and constructor

        static IWebDriver driver = new ChromeDriver();
        static WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));


        #endregion


        protected IWebElement WaitForElementExists(By locator)
        {
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(locator));
            //IWebElement element = driver.FindElement(locator);
            // wait.Until(ExpectedConditions.ElementToBeClickable(element));
            return element;

        }

        protected IWebElement WaitForElementClickable(By locator)
        {
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(locator));
            //IWebElement element = driver.FindElement(locator);
            // wait.Until(ExpectedConditions.ElementToBeClickable(element));
            return element;

        }

        protected IWebElement WaitForElementIsVisible(By locator)
        {
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(locator));
            //IWebElement element = driver.FindElement(locator);
            // wait.Until(ExpectedConditions.ElementToBeClickable(element));
            return element;

        }
        public void Click(By locator)
        {
            driver.FindElement(locator).Click();
        }

        public void SendKeys(By locator, string text)
        {
            driver.FindElement(locator).SendKeys(text);
        }

        public void NavigateTo(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public void Quit(){
            driver.Quit();
        }

    }

}

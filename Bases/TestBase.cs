using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ProjetoNUnit.Bases
{
    public class TestBase : PageBase
    {
        static IWebDriver  driver = new ChromeDriver();
         // [SetUp]
      //  public void Setup()
       // {
         //   driver = new ChromeDriver();
      //  }

        [TearDown]
        public void TearDown()
        {
           driver.Quit();
        }

    }
}
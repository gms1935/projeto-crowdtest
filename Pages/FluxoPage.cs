

using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using OpenQA.Selenium;
using ProjetoNUnit.Bases;

namespace ProjetoNUnit.Pages
{
    public class FluxoPage : PageBase
    {
        #region Mapping

        static string urlCrowdTest = "http://blackmirror.crowdtest.me.s3-website-us-east-1.amazonaws.com/auth";
        By usernameField = By.Id("login");
        By passwordField = By.Id("password");
        By loginButton = By.XPath("//div/button[@class='btn btn-crowdtest btn-block']");

        By menuButton = By.XPath("//div/*[@class='toggle-menu-btn open-menu-btn']");
        By projetosButton = By.XPath(".//*[@class='li-projects']");
        By projetoCriadoButton = By.TagName("mat-cell");

        By casosTesteButton = By.Id("mat-tab-label-0-2");

        By rowButton = By.XPath(".//*[@class='mat-cell cdk-column-title mat-column-title ng-star-inserted']");

        #endregion

        #region Actions

        public void AcessarUrlSite()
        {
            NavigateTo(urlCrowdTest);
        }
        
        public void PreencherUsuario(string usuario)
        {
            SendKeys(usernameField, usuario);
        }

        public void PreencherSenha(string senha)
        {
            SendKeys(passwordField, senha);
        }

        public void ClicarEmLogin()
        {
            Click(loginButton);
        }

        public void ClicarEmMenuButton()
        {
            WaitForElementIsVisible(menuButton).Click();
        }

        public void ClicarEmProjetosButton()
        {
            Click(projetosButton);
        }

        public void ClicarEmProjetosCriadosButton()
        {
            WaitForElementIsVisible(projetoCriadoButton).Click();
        }

        public void ClicarEmCasosTesteButton()
        {
            WaitForElementExists(casosTesteButton).Click();
        }

        public IWebElement ClicarEmRowButton()
        {
            return WaitForElementClickable(rowButton);
        }
        #endregion  

    }
}
